﻿using System;
using System.Configuration;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Sentry;

namespace App.Web {
    public class MvcApplication : HttpApplication {

        private IDisposable _sentry;

        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            WebApiConfig.Register(GlobalConfiguration.Configuration);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.InitMapper();
            HangfireBootstrapper.Instance.Start();
            log4net.Config.XmlConfigurator.Configure();

            // Set up the sentry SDK
            _sentry = SentrySdk.Init(o =>
            {
                o.Dsn = new Dsn(ConfigurationManager.AppSettings["SentryDsn"]);
            });

        }


        protected void Application_Error()
        {
            var exception = Server.GetLastError();

            // Capture unhandled exceptions
            SentrySdk.CaptureException(exception);
        }

        protected void Application_End(object sender, EventArgs e)
        {
            HangfireBootstrapper.Instance.Stop();
            _sentry?.Dispose();
        }


       
    }
}