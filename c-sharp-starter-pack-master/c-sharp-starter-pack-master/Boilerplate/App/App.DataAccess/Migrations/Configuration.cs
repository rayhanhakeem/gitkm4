namespace App.DataAccess.Migrations
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<App.DataAccess.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(App.DataAccess.ApplicationDbContext context)
        {
            var movieSeed = new List<Movie>();
            movieSeed.Add(
                new Movie()
                {
                    Name = "Charlie and The Chocolate Factory",
                    Category = "Everyone",
                    Language = "English",
                    ReleaseDate = new DateTime(201123091230912)
                });
            movieSeed.Add(
                new Movie()
                {
                    Name = "Deadpool",
                    Category = "Adult",
                    Language = "English",
                    ReleaseDate = new DateTime(12939123913)
                });
            movieSeed.Add(
                new Movie()
                {
                    Name = "Laskar Pelangi",
                    Category = "Everyone",
                    Language = "Indonesia",
                    ReleaseDate = new DateTime(12301239012)
                });

            var theatreSeed = new List<Theatre>();
            theatreSeed.Add(
                new Theatre()
                {
                    Name = "Theatre 1",
                    NumberOfSeat = "100",
                });
            theatreSeed.Add(
                new Theatre()
                {
                    Name = "Theatre 2",
                    NumberOfSeat = "50",
                });
            theatreSeed.Add(
                new Theatre()
                {
                    Name = "Theatre 1",
                    NumberOfSeat = "25",
                });

            var showSeed = new List<ShowTime>();
            showSeed.Add(
                new ShowTime()
                {
                    Date = new DateTime(2020, 5, 21),
                    Price = 100000,
                    Movie = movieSeed[1],
                    Theatre = theatreSeed[0]
                });
            showSeed.Add(
                new ShowTime()
                {
                    Date = new DateTime(2020, 5, 22),
                    Price = 150000,
                    Movie = movieSeed[0],
                    Theatre = theatreSeed[0]
                });
            showSeed.Add(
                new ShowTime()
                {
                    Date = new DateTime(2020, 5, 21),
                    Price = 120000,
                    Movie = movieSeed[2],
                    Theatre = theatreSeed[1]
                });
            if (!context.Movies.Any())
            {
                foreach (var i in movieSeed)
                {
                    context.Movies.AddOrUpdate(i);
                }
            }
            if (!context.Theatres.Any())
            {
                foreach (var i in theatreSeed)
                {
                    context.Theatres.AddOrUpdate(i);
                }
            }
            if (!context.ShowTimes.Any())
            {
                foreach (var i in showSeed)
                {
                    context.ShowTimes.AddOrUpdate(i);
                }
            }
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.SaveChanges();
        }
    }
}
