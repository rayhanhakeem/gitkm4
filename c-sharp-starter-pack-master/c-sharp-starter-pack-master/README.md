![](https://gitlab.com/suitmedia/c-sharp-starter-pack/-/raw/master/docs/logo.png)

### Overview ###

C Sharp Starter Pack dapat digunakan untuk base code/framework membuat aplikasi berbasis ASP .Net MVC 5 dengan .Net Frameworks minimal 4.6.1. 
Dalam source ini sudah disertakan sample project pemesanan tiket bioskop sederhana.

Dokumentasi lebih lengkap penggunaan *C Sharp Starter Pack* dapat dilihat di [wiki](https://gitlab.com/suitmedia/c-sharp-starter-pack/-/wikis/home)

### Getting Started ###
Untuk memulai development project ini terdapat beberapa applikasi yang perlu diinstall terlebih dahulu :

- Visual Studio mininal 2017 [*ASP .Net and web development* enabled](https://docs.microsoft.com/en-us/visualstudio/install/modify-visual-studio?view=vs-2017)
- .Net framework 4.6.1
- PostgreSQL 11

#### Code Setup ####
Apabila sudah terpenuhi kemudian ikuti langkah berikut :

- Clone repository ini
- Buka solution ```c-sharp-starter-pack\Boilerplate\App\App.sln```
- Pada window *Solution Explorer* klik kanan pada solution lalu pilih ``` Restore NuGet Packages ```

![](https://gitlab.com/suitmedia/c-sharp-starter-pack/-/raw/master/docs/Solution.JPG)

![](https://gitlab.com/suitmedia/c-sharp-starter-pack/-/raw/master/docs/RestorePackage.jpg)

- Kemudian ```Build > Build Solution ``` atau ``` F6 ```
- Tunggu proses build sambil berdoa agar proses build berjalan lancar :pray:  

#### Database Setup ####
apabila build sudah berhasil, langkah berikutnya adalah menyiapkan database. berikut ini langkah langkah menyiapkan database :

- Buat database baru
- Buka Web.config
- ganti *value* pada section ```<connectionStrings>``` dengan name ```DefaultConnection``` sesuai database yang baru dibuat
- Buka menu ``` Tools > NuGet Package Manager > Package Manager Console ```
- Danti default project menjadi ``` DataAccess\App.DataAccesss```
- Pada console jalankan perintah ```Update-Database```
- Tunggu proses migrasi skema sampai selesai
- Jalankan aplikasi dengan menekan tombol ```F6``` atau melalui menu ```Debug > Start Debugging```
- Login sebagai admin dengan menggunakan user ```admin@app.net``` dan password ```Aspapp@789```
Happy Developing :metal:

#### Sample Project
Sample project yang sudah include pada project ini adalah contoh aplikasi sederhana order tiket bioskop. untuk memudahkan pemahaman penggunaan framework ini. dapat difork dari branch ```master```

#### Bare Project
Apabila tidak membutuhkan sample atau hanya base code saja, dapat meng-fork dari branch ```bare-master```

### Stack ###
aplikasi menggunakan beberapa plugins dengan spesifikasi dan penjelasan sebagai berikut :

#### Entity Framework ####
Digunakan sebagai ORM pada DataAccess dan Integrasi DSAS

#### Npgsql ####
Digunakan sebagai data provider untuk PostgreSQL

#### Hangfire ####
Digunakan untuk me-manage *background job* dan *cron job*.

#### Ninject ####
Digunakan untuk melakukan dependency injection.

#### OpenXML ####
Digunakan untuk melakukan proses read/write file .xlsx dan proses generate dokumen .docx.

#### AutoMapper ####
AutoMapper secara umum digunakan untuk melakukan mapping secara otomatis property object Model ke ViewModel atau sebaliknya.

#### Elmah ####
Elmah dalam project aplikasi digunakan untuk melakukan error tracking. 

#### Swagger ####
Swagger berfungsi untuk mendokumentasikan API yang tersedia di project App.Web. halaman swagger dapat diakses melalui url ```/swagger```

### Development ###
Proses development fitur kebanyakan akan dilakukan dalam project DataAccess untuk mengubah skema Database dan Web untuk logic dan view. 

> **Tips** Untuk memudahkan melakukan navigasi melihat isi sebuah method, gunakan kombinasi keyboard <kbd>ctrl</kbd> + <kbd>f12</kbd> atau klik kanan ```Go To Implementation``` agar diarahkan ke implementasi dari method tersebut, bukan ke interface nya

### Deployment ###
#### Azure Deployment ####
Deployment ke server Azure dilakukan dengan menggunakan file publish profile yang di dapat dari ```Portal Azure```. lebih lengkap mengenai deployment ke azure dapat dilihat [di sini](https://docs.microsoft.com/en-us/visualstudio/deployment/tutorial-import-publish-settings-azure?view=vs-2019).
sebelum melakukan deploy pastikan versi aplikasi sudah diubah pada file ```App.Web\Utilities\CommonHelper.cs```. Ubah versi aplikasi pada method ```GetVersion()``` panduan melakukan perubahan nama versi dapat dilihat [di sini](#versioning-guide)

#### Versioning Guide ####
Berikut ini adalah panduan melakukan update versi. contoh format versioning pada aplikasi adalah ```v1.4.13```. 
- angka ```1``` adalah fase pengembangan aplikasi. sampai dokumen ini dibuat fase aplikasi masih pada fase 1.
- angka ```4``` adalah increment sesuai jumlah deployment ke server Production yang di-reset apabila digit pertama berubah
- angka ```13``` adalah increment sesuai jumlah deployment ke server staging yang akan di-reset apabila digit ke 2 berubah

### Contributing
Apabila anda merasa framework ini berguna dan ingin ikut mengembangkan silahkan ikuti [Contributing Guide ini](https://gitlab.com/suitmedia/c-sharp-starter-pack/-/wikis/Contributing)
